<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>BookSale</title>
		<asset:stylesheet src="reen_theme.css"/>
		
		<g:layoutHead/>
		<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<h1>Hola mundo</h1>
		<!-- JavaScripts placed at the end of the document so the pages load faster -->
		
		<!-- For demo purposes – can be removed on production -->
		
		<g:layoutBody/>

	</body>
</html>
